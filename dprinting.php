<!DOCTYPE html>
<html lang="en">

<?php include 'head.php'; ?>
<body>

  <!-- ***** Preloader Start ***** -->
  <div id="preloader">
    <div class="jumper">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->


  <!-- ***** Header Area Start ***** -->
  <?php include 'header.php'; ?>
  <!-- ***** Header Area End ***** -->



  <!-- ***** Main Banner Area Start ***** -->
  <div class="main-banner header-text" id="top">
    <div class="Modern-Slider">
      <!-- Item -->
      <div class="item">
        <div class="img-fill">
          <img src="assets/images/dprint1.jpg" alt="">
          <div class="text-content">
            <h3>PRODUCTS</h3>
            <h5>SBMB Digital Printing</h5>
          </div>
        </div>
      </div>
      <!-- // Item -->


    </div>
  </div>
  <div class="scroll-down scroll-to-section"><a href="#about"><i class="fa fa-arrow-down"></i></a></div>
  <!-- ***** Main Banner Area End ***** -->

  <section class="section" id="about">
    <div class="container" style="margin-top: 80px;">
      <div class="row" style="margin-top: -150px;">
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="left-text-content">
            <div class="section-heading" style="text-align: center;">
              <h2>SBMB Digital Printing</h2><br>
            </div>
            <div class="right-text-content" style="margin-top: 20px;">
              <p style="color: black; font-size: medium; text-align: justify;">Di tahun ke ... SBMB meluaskan bentangan
                sayapnya dengan melahirkan
                anak perusahaan baru dalam lingkup yang berbeda dari sebelumnya.
                Bidang baru ini menggunakan metode dalam percetakan modern
                yang melibatkan teknik digital sebagai media transfer antara
                materi ke media percetakan. SBMB Digital Printing merupakan salah satu anak perusahaan
                dari SBMB yang bergerak di bidang printing & advertising.
                Kami menyediakan layanan untuk kebutuhan printing yang
                nantinya bisa digunakan untuk sarana promosi dan lain sebagainya.
                SBMB Digital Printing mulai berdiri tahun .... Karena masih terbilang baru,
                kami selalu berusaha meningkatkan kualitas dan juga persebaran produk kami.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12" style="text-align: center;">
          <div class="section-heading">
            <h2>Lokasi</h2><br>
          </div>
          <div class="right-text-content" style="text-align: center;">
            <!-- <p style="color: black; font-size: medium; text-align: justify;"> Jl. Mayjen Sungkono No.36, Bumiayu, Kec.
              Kedungkandang, Kota Malang, Jawa Timur 65135
            </p> -->
            <br>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15802.96542069135!2d112.642608!3d-8.0255829!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3451d0a5545123ce!2sPT%20Sarana%20Berkah%20Maju%20Bersama!5e0!3m2!1sid!2sid!4v1642597703782!5m2!1sid!2sid"
              width="500" height="250" style="border: 1px solid black; border-radius: 10px; margin-top: -30px;" allowfullscreen=""
              loading="lazy"></iframe>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ***** About Area Starts ***** -->

  <!-- Card Start-->
  <!-- <hr width="90%" style="margin-top: 60px;"> -->
    <div class="section-heading">
      <h2 style="text-align: center;"><br>Produk</h2>
    </div>
    <h5 style="text-align: center; font-size: medium;">Produk Digital Printing SBMB</h5>
    <br>
    <div class="container">
      <div class="row">
        
        <?php include "koneksi.php";
        $query = mysqli_query($conn, 'SELECT * FROM printing');
        $result = array();
        while ($data = mysqli_fetch_array($query)){
          $result[] = $data;
        }

        foreach ($result as $value){
        ?>
        
        <div class="card" style="margin-left: 70px; 
        margin-top: 30px; margin-bottom: 30px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); 
         font-family: arial; max-width: 300px;">
          <input type="image" style="height: 300px; width:300px" src="assets/images/<?php echo $value['gambar'];?>" data-toggle="modal" data-target="#ModalBanner">
          <div class="container" style="margin-top: 10px;">
            <h2 style="color:black; text-align: center;"><?php echo $value['nama_produk']?></h2>
            <p style="color: grey; font-size:18px; margin-top:10px; text-align: center;">Rp. <?php echo $value['harga_produk']?></p>
            <p style="color:black;font-size:16px; margin-top:20px;">Ukuran: <?php echo $value['ukuran']?></p>
            <p style="color:black;font-size:16px; margin-bottom:20px;">Bahan Produk: <?php echo $value['bahan_produk']?></p>
          </div>
        </div>
        <?php } ?>
       </div>
    </div>
  

  <!-- ***** Footer Start ***** -->
  <?php include 'footer.php'; ?>


  <!-- jQuery -->
  <script src="assets/js/jquery-2.1.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/popper.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Plugins -->
  <script src="assets/js/owl-carousel.js"></script>
  <script src="assets/js/scrollreveal.min.js"></script>
  <script src="assets/js/waypoints.min.js"></script>
  <script src="assets/js/jquery.counterup.min.js"></script>
  <script src="assets/js/imgfix.min.js"></script>
  <script src="assets/js/slick.js"></script>
  <script src="assets/js/lightbox.js"></script>
  <script src="assets/js/isotope.js"></script>

  <!-- Global Init -->
  <script src="assets/js/custom.js"></script>

  <script>

    $(function () {
      var selectedClass = "";
      $("p").click(function () {
        selectedClass = $(this).attr("data-rel");
        $("#portfolio").fadeTo(50, 0.1);
        $("#portfolio div").not("." + selectedClass).fadeOut();
        setTimeout(function () {
          $("." + selectedClass).fadeIn();
          $("#portfolio").fadeTo(50, 1);
        }, 500);

      });
    });

  </script>

</body>

</html>