<!DOCTYPE html>
<html lang="en">

<?php include 'head.php'; ?>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <?php include 'header.php'; ?>
    <!-- ***** Header Area End ***** -->



    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="assets/images/bmsfilterrods5.jpg" alt="">
                    <div class="text-content">
                        <h3>PRODUCTS</h3>
                        <h5>SBMB Filterrods</h5>
                    </div>
                </div>
            </div>
            <!-- // Item -->


        </div>
    </div>
    <div class="scroll-down scroll-to-section"><a href="#about"><i class="fa fa-arrow-down"></i></a></div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xs-12">
                    <div class="section-heading">
                        <h2>Overview </h2>
                        <!-- <h2>SBMB Digital Printing</h2> -->
                    </div>
                    <div class="right-text-content">
                        <p style="color: black; text-align: justify; font-size: large;">
                            SBMB adalah perusahaan Joint Venture
                            dari 2 entitas lebih yang bertujuan untuk
                            memenuhi kebutuhan Filter Rod dari dua grup
                            rokok yang ada di Malang yaitu ‘Gudang Baru
                            Group’ dan ‘Cakra Guna Karya Nusa’. Pabrik Gadang, didirikan bersamaan dengan
                            berdirinya SBMB pada tahun 2011 yang berlokasi di Jalan Mayjend
                            Sungkono, Nomor 36, Kedungkandang, Kota Malang. Pabrik Gadang
                            memproduksi Filter Rod dengan kapasitas per bulannya mencapai 14.250
                            tray filter reguler dan 17.250 tray filter bold.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xs-12">
                    <div class="section-heading">
                        <!-- <h6>More About Product</h6> -->
                        <h2>SBMB Filterrods</h2>
                    </div><br>
                </div>
                <div class="row">
                <?php include "koneksi.php";
                $query = mysqli_query($conn, 'SELECT * FROM filterrods');
                $result = array();
                while ($data = mysqli_fetch_array($query)) {
                    $result[] = $data;
                }

                foreach ($result as $value) {
                ?>
                    <div class="col-md-6 col-lg-4">
                        <div class="col-md-12">
                            <a href="filterrods1.php">
                                <img src="assets/images/<?php echo $value['gambar'];?>" style="width: 450px; height: 300px;">
                            </a>
                            <div class="text bg-white p-2">
                                <h3 class="heading" style="margin-top: 15px;"><?php echo $value['nama_produk'];?></h3><br>
                                <p style="color: black; text-align: justify; font-size: medium;">
                                <?php echo $value['deskripsi'];?>"</p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-md-6 col-lg-4">
                        <div class="col-md-12">
                            <div class="text bg-white p-2">
                                <div class="d-flex align-items-center mt-4">
                                    <p class="mb-0"><a href="filterrods1.php" class="btn btn-primary">Read More <span
                                                class="ion-ios-arrow-round-forward"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="col-md-12">
                            <div class="text bg-white p-2">
                                <div class="d-flex align-items-center mt-4">
                                    <p class="mb-0"><a href="filterrods2.php" class="btn btn-primary">Read More <span
                                                class="ion-ios-arrow-round-forward"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="col-md-12">
                            <div class="text bg-white p-2">
                                <div class="d-flex align-items-center mt-4">
                                    <p class="mb-0"><a href="filterrods3.php" class="btn btn-primary">Read More <span
                                                class="ion-ios-arrow-round-forward"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- gallery -->
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="section-heading" style="text-align: center;">
                <br>
                <h2>Gallery Filterrods</h2>
            </div>
            <div class="subscribe-content" style="text-align: center;">
                <p>kumpulan gambar produk dan fasilitas dari filterrods</p>
                <br>
            </div>
        </div>
    </div>

    
    <ul class="gallery_box">
    <?php include "koneksi.php";
        $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=1 LIMIT 6');
        $result = array();
        while ($data = mysqli_fetch_array($query)){
        $result[] = $data;
        } foreach ($result as $row){
    ?>
        <li>
            <img src="assets/images/<?php echo $row['gambar'];?>" width="600" height="400"></a>
        </li>
        <?php } ?>
    </ul>
    
    <!-- gallery end -->

    
    <br><br>
    <!-- ***** Footer Start ***** -->
    <?php include 'footer.php'; ?>

    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

    <script>

        $(function () {
            var selectedClass = "";
            $("p").click(function () {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut();
                setTimeout(function () {
                    $("." + selectedClass).fadeIn();
                    $("#portfolio").fadeTo(50, 1);
                }, 500);

            });
        });

    </script>

</body>

</html>