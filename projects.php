<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'; ?>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <?php include 'header.php'; ?>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="assets/images/slide-04.png" alt=""style="opacity: 0.8;">
                    <div class="text-content">
                    
                    <h3>PROYEK</h3>
                    <h5>PT. Sarana Berkah Maju Bersama</h5>
                    <p style="font-size: 20pt; color: white; margin-top: -20px;">"produk & proyek kami"</p>
                    </div>
                </div>
            </div>
            <!-- // Item -->


        </div>
    </div>
    <div class="scroll-down scroll-to-section"><a href="#testimonials"><i class="fa fa-arrow-down"></i></a></div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** Testimonials Starts ***** -->
    <section class="section" id="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h6>Produk & Proyek</h6>
                        <h2>PT. Sarana Berkah Maju Bersama</h2>
                    </div>
                </div>
                    <?php include "koneksi.php";
                    $query = mysqli_query($conn, 'SELECT * FROM project');
                    $result = array();
                    while ($data = mysqli_fetch_array($query)) {
                    $result[] = $data;
                    }

                    foreach ($result as $value) {
                    ?>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <div class="features-item">
                        <div class="features-icon">
                            <img src="assets/images/<?php echo $value['gambar'];?>" width="330px" height="300px" alt=""><br><br>
                        </div>
                        <div class="features-content">
                            <h4 style="font-size: large; color:white;"><?php echo $value['nama_kategori'];?></h4>
                            <span style="color: white;">
                            <?php echo $value['deskripsi'];?>
                            </span>
                        </div>
                    </div>
                </div>
                    <?php } ?>
            </div>
        </div>
    </section>
    <!-- ***** Testimonials Ends ***** -->
    <!-- ***** About Area Starts ***** -->
    <section class="section" id="projects">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="section-heading">
                        <h6>Proyek #1</h6>
                        <h2>Beberapa produk & proyek  kami</h2>
                    </div>
                    <div class="filters">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".des">Filterrods</li>
                            <li data-filter=".dev">Aluminium Paper</li>
                            <li data-filter=".gra">Digital Printing</li>
                            <!--<li data-filter=".tsh">Artworks</li> -->
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="filters-content">
                        <div class="row grid">
                        <?php include "koneksi.php";
                            $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=1');
                            $result = array();
                            while ($data = mysqli_fetch_array($query)){
                            $result[] = $data;
                            } foreach ($result as $row){
                        ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 all des">
                                <div class="item">
                                    <a href="assets/images/<?php echo $row['gambar'];?>" data-lightbox="image-1"
                                        data-title="Filterrods Produk">
                                        <img src="assets/images/<?php echo $row['gambar'];?>" alt=""></a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php include "koneksi.php";
                            $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=3');
                            $result = array();
                            while ($data = mysqli_fetch_array($query)){
                            $result[] = $data;
                            } foreach ($result as $row){
                        ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 all gra">
                                <div class="item">
                                    <a href="assets/images/<?php echo $row['gambar'];?>" data-lightbox="image-1"
                                        data-title="Digital Printing - Banner">
                                        <img src="assets/images/<?php echo $row['gambar'];?>" alt=""></a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php include "koneksi.php";
                            $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=2');
                            $result = array();
                            while ($data = mysqli_fetch_array($query)){
                            $result[] = $data;
                            } foreach ($result as $row){
                        ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 all dev">
                                <div class="item">
                                    <a href="assets/images/<?php echo $row['gambar'];?>" data-lightbox="image-1"
                                        data-title="Grenjeng Paper / Alumunium Foil">
                                        <img src="assets/images/<?php echo $row['gambar'];?>"
                                            alt=""></a>
                                </div>
                            </div>
                        <?php } ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- ***** Footer Start ***** -->
    <?php include 'footer.php'; ?>

    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

    <script>

        $(function () {
            var selectedClass = "";
            $("p").click(function () {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut();
                setTimeout(function () {
                    $("." + selectedClass).fadeIn();
                    $("#portfolio").fadeTo(50, 1);
                }, 500);

            });
        });

    </script>

    <script>
        var slideIndex = 0;
        showSlides();

        function showSlides() {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > slides.length) { slideIndex = 1 }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
            setTimeout(showSlides, 2000); // Change image every 2 seconds
        }
    </script>
</body>

</html>