
<!DOCTYPE html>
<html lang="en">

<?php include 'head.php'; ?>

<?php

include 'koneksi.php';
//menangkap data yang dikirim dari form login

if (isset($_POST["add"])) { //jika tombol send messagedi klik

    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $pesan = $_POST['pesan'];
    
  

                $sqlsimpan = mysqli_query($conn, "INSERT INTO contact (nama, email, pesan)
	 VALUES
	  (
	  '$nama',
    '$email',
	  '$pesan')") or die(mysqli_error($conn));

                if ($sqlsimpan) { // Cek jika proses simpan ke database sukses atau tidak
                    // Jika Sukses, Lakukan :
                    echo "<script>alert('Message sent! Thank you for contacting us.')</script>";
                    header("location:index.php"); // Redirectke halaman index.php
                    // return 'dprinting.html';
                } else {
                    // Jika Gagal, Lakukan :
                    echo mysqli_error();
                }
            
}


?>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <?php include 'header.php'; ?>
    <!-- ***** Header Area End ***** -->
    
    

    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
          <!-- Item -->
          <div class="item">
            <div class="img-fill">
                <img src="assets/images/slide-04.png" alt="" style="opacity: 0.8;">
                <div class="text-content">
                  <h3>CONTACT US</h3>
                  <h5>PT. Sarana Berkah Maju Bersama</h5>
                </div>
            </div>
          </div>
          <!-- // Item -->
          
          
        </div>
    </div>
    <div class="scroll-down scroll-to-section"><a href="#contact-us"><i class="fa fa-arrow-down"></i></a></div>
    <!-- ***** Main Banner Area End ***** -->

    
    <!-- ***** Contact Us Area Starts ***** -->
    
    <section class="section" id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="left-text-content">
                        <div class="section-heading">
                            
                            <h2>Feel free to keep in touch with us!</h2>
                        </div>
                        <ul class="contact-info">
                            <li><img src="assets/images/contact-info-01.png" alt="">0341-751116</li>
                            <li><a href="mailto:bcsbmb@gmail.com?Subject=Message From User" style="color:black;"><img src="assets/images/contact-info-02.png" alt="">bcsbmb@gmail.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="contact-form">
                        <form  action="" method="POST" enctype="multipart/form-data">
                          <div class="row">
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="nama" type="text" id="nama" placeholder="Your Name *" required="">
                              </fieldset>
                            </div>
                            
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="email" type="email" id="email" placeholder="Your Email *" required="">
                              </fieldset>
                            </div>
                            
                            <div class="col-lg-12">
                              <fieldset>
                                <textarea name="pesan" rows="6" id="pesan" placeholder="Message" required=""></textarea>
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" name="add"  class="main-button-icon">Send Message Now <i class="fa fa-arrow-right"></i></button>
                              </fieldset>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Contact Us Area Ends ***** -->
    
    <!-- ***** Footer Start ***** -->
    <?php include 'footer.php'; ?>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    <script src="assets/js/slick.js"></script> 
    <script src="assets/js/lightbox.js"></script> 
    <script src="assets/js/isotope.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

    <script>

        $(function() {
            var selectedClass = "";
            $("p").click(function(){
            selectedClass = $(this).attr("data-rel");
            $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("."+selectedClass).fadeOut();
            setTimeout(function() {
              $("."+selectedClass).fadeIn();
              $("#portfolio").fadeTo(50, 1);
            }, 500);
                
            });
        });

    </script>

  </body>
</html>