<!DOCTYPE html>
<html lang="en">

<?php include 'head.php'; ?>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <?php include 'header.php'; ?>
    <!-- ***** Header Area End ***** -->



    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="assets/images/paper7.jpg" alt="">
                    <div class="text-content">
                        <h3>PRODUCTS</h3>
                        <h5>Alumunium Foil Paper</h5>
                    </div>
                </div>
            </div>
            <!-- // Item -->


        </div>
    </div>
    <div class="scroll-down scroll-to-section"><a href="#about"><i class="fa fa-arrow-down"></i></a></div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <?php include "koneksi.php";
                $query = mysqli_query($conn, 'SELECT * FROM aluminium');
                $result = array();
                while ($data = mysqli_fetch_array($query)) {
                    $result[] = $data;
                }

                foreach ($result as $value) {
                ?>
                    <div class="col-lg-12 col-md-6 col-xs-12">
                        <div class="section-heading">
                            <!-- <h2>Alumunium Foil</h2> -->
                            <h2><?php echo $value['nama_produk'] ?></h2>
                        </div>
                        <div class="right-text-content">
                            <p style="color: black; font-size: large; text-align: justify;">
                                <?php echo $value['deskripsi'] ?>
                            </p><br>
                            <p style="color: black; font-size: large; text-align: justify;">
                                <b> Rp. <?php echo $value['harga_produk'] ?> (didiskusikan secara personal) </b>
                            </p>
                        </div><br><br>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- gallery -->
    <div class="row" style="margin-top: 60pt;">
        <div class="col-lg-8 offset-lg-2">
            <div class="section-heading" style="text-align: center;">
                <br>
                <h2>Gallery Alumunium Foil Paper</h2>
            </div>
            <div class="subscribe-content" style="text-align: center;">
                <br>
                <p style="color: black; font-size: medium;">kumpulan gambar produk dari alumunium foil paper</p>
                <br>
            </div>
        </div>
    </div>

    <ul class="gallery_box">
    <?php include "koneksi.php";
        $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=2 LIMIT 6');
        $result = array();
        while ($data = mysqli_fetch_array($query)){
        $result[] = $data;
        } foreach ($result as $row){
    ?>
        <li>
            <img src="assets/images/<?php echo $row['gambar'];?>" width="600" height="400"></a>
        </li>
        <?php } ?>
    </ul>
    <!-- gallery end -->

    <!-- ***** Footer Start ***** -->
    <?php include 'footer.php'; ?>


    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

    <script>
        $(function() {
            var selectedClass = "";
            $("p").click(function() {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut();
                setTimeout(function() {
                    $("." + selectedClass).fadeIn();
                    $("#portfolio").fadeTo(50, 1);
                }, 500);

            });
        });
    </script>

</body>

</html>