<div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
          <div class="container-fluid">
            <a class="navbar-brand font-weight-bolder ms-lg-0 ms-3 " >
              Admin SBMB
            </a>
            
            <div class="collapse navbar-collapse" id="navigation">
              <ul class="navbar-nav mx-auto">
              <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../admin/tabelproject.php">
                    <i class="fa fa-tasks opacity-6 text-dark me-1"></i>
                    Project
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../admin/tabelfilterrods.php">
                    <i class="fa fa-window-minimize opacity-6 text-dark me-1"></i>
                    Filterrods
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../admin/tabelaluminium.php">
                    <i class="fa fa-bullseye opacity-6 text-dark me-1"></i>
                    Alumunium Paper
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../admin/tabeldprinting.php">
                    <i class="fa fa-print opacity-6 text-dark me-1"></i>
                    Printing
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../admin/tabelgambar.php">
                    <i class="fa fa-picture-o opacity-6 text-dark me-1"></i>
                    Images
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../admin/tabelcontact.php">
                    <i class="fa fa-phone opacity-6 text-dark me-1"></i>
                    Contact
                  </a>
                </li>
              </ul>
              <ul class="navbar-nav d-lg-block d-none">
                <li class="nav-item">
                  <a href="../logout.php" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-dark">Logout</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
      </div>
    </div>
  </div>
  <br><br><br><br>