<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../admin/assets/img/logo.png">
  <link rel="icon" type="image/png" href="../admin/assets/img/logo.png">
  <title>
    Admin SBMB
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="../admin/assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../admin/assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="../admin/assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="../admin/assets/css/soft-ui-dashboard.css?v=1.0.3" rel="stylesheet" />
</head>

<body class="">

  <?php include '../koneksi.php';
  
 error_reporting(0);
  
 session_start();
  
 if (isset($_SESSION['username'])) {
     header("Location: ../admin/tabelfilterrods.php");
 }
  
 if (isset($_POST['login'])) {
     $username = $_POST['username'];
     $password = md5($_POST['password']);
  
     $sql = "SELECT * FROM user WHERE username='$username' AND password='$password'";
     $result = mysqli_query($conn, $sql);
     if ($result->num_rows > 0) {
         $row = mysqli_fetch_assoc($result);
         $_SESSION['username'] = $row['username'];
         echo "<script> alert('Selamat Datang Admin!'); window.location.href='../admin/tabelfilterrods.php'; </script>";
     } else {
         echo "<script>alert('Email atau password Anda salah. Silahkan coba lagi!')</script>";
     }
 }
  
 ?>

  <div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
          <div class="container-fluid">
            
            <div class="collapse navbar-collapse" id="navigation">
              <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../index.php">
                    <i class="fa fa-chart-pie opacity-6 text-dark me-1"></i>
                    Home
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="../about.php">
                    <i class="fa fa-user opacity-6 text-dark me-1"></i>
                    About
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="../projects.php">
                    <i class="fas fa-align-justify opacity-6 text-dark me-1"></i>
                    Product
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="../filterrods.php">
                    <i class="fas fa-chevron-right opacity-6 text-dark me-1"></i>
                    Filterrods
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="../alumuniumpaper.php">
                    <i class="fas fa-chevron-right opacity-6 text-dark me-1"></i>
                    Alumunium Paper
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="../dprinting.php">
                    <i class="fas fa-chevron-right opacity-6 text-dark me-1"></i>
                    Printing
                  </a>
                </li>
              </ul>
              <ul class="navbar-nav d-lg-block d-none">
                <li class="nav-item">
                  <a href="../index.php" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-dark">Back</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
      </div>
    </div>
  </div>
  <main class="main-content  mt-0">
    <section>
      <div class="page-header min-vh-75">
        <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
              <div class="card card-plain mt-8">
                <div class="card-header pb-0 text-left bg-transparent">
                  <h3 class="font-weight-bolder text-info text-gradient">Welcome Back - Admin SBMB (Only)</h3>
                  <p class="mb-0">Enter your username and password</p>
                </div>
                <div class="card-body">
                  <form role="form" action="" method="POST" enctype="multipart/form-data">
                    <label>Username</label>
                    <div class="mb-3">
                      <input type="username" id="username" name="username" class="form-control" placeholder="Username" aria-label="username" aria-describedby="user-addon">
                    </div>
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <div class="mb-3">
                      <input type="password" id="password" name="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="password-addon">
                    </div>
                    <div class="text-center">
                      <button type="submit" name="login" class="btn bg-gradient-info w-100 mt-4 mb-0">Sign in</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('../admin/assets/img/bmsfilterrods4.jpg')"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  
  <!--   Core JS Files   -->
  <script src="../admin/assets/js/core/popper.min.js"></script>
  <script src="../admin/assets/js/core/bootstrap.min.js"></script>
  <script src="../admin/assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="../admin/assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../admin/assets/js/soft-ui-dashboard.min.js?v=1.0.3"></script>
</body>

</html>