<?php

session_start();

if (!isset($_SESSION['username'])) {
  header("Location: ../admin/tabelproject.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<?php include "head.php";?>
<!-- body -->

<body>
  <?php include "navbar.php";?>

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Tabels</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Project</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Project</h6>
        </nav>
        <form action="tabelproject.php" method="GET">
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group">
              <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
              <input type="text" name="cari" class="form-control" placeholder="Type here...">
            </div>
            <input type="submit" style="margin-left:5px;" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-dark" value="Cari">
          </div>
        </div>
        </form>
        <?php if(isset($_GET['cari'])){
          $cari = $_GET['cari'];
        }?>
      </div>
    </nav>
    <!-- End Navbar -->


    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="col-4 text-start" style="margin-left:15px;">
            <a href="../admin/tambahproject.php">
              <button type="button" class="fas fa-plus icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
              </button>
            </a>
          </div>
        </div>
      </div>

      <div class="row mt-3">
        <div class="row my-3">
          <div class="col-lg-12 col-md-10 mb-md-0 mb-8">
            <div class="card">
              <div class="card-body px-0 pb-2">
                <div class="table-responsive" style="margin-left:15px;">
                  <table class="table align-items-center mb-0">
                    <thead>
                      <tr>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"><b>Nama</b></th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"><b>Deskripsi</b></th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"></th>
                      </tr>
                    </thead>
                    <?php
                    include '../koneksi.php';

                    if(isset($_GET['cari'])){
                      $cari=$_GET['cari'];
                      $query = "SELECT*FROM project WHERE nama_kategori LIKE '%".$cari."%'";
                      $result = mysqli_query($conn, $query);
                    } else{
                    $query = "SELECT*FROM project";
                    $result = mysqli_query($conn, $query);
                    }
                    if (mysqli_num_rows($result) > 0) {
                      while ($row = mysqli_fetch_assoc($result)) {
                    ?>
                        <tbody>
                          <tr>
                          
                            <td>
                              <div class="d-flex px-2 py-1">
                                <div>
                                  <img src="../assets/images/<?php echo $row['gambar']; ?>" class="avatar avatar-sm me-3" alt="xd">
                                </div>
                                <div class="d-flex flex-column justify-content-center">
                                  <h6 class="mb-0 text-sm"><?php echo $row["nama_kategori"] ?></h6>
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="avatar-group mt-2">
                                <?php echo $row["deskripsi"] ?>
                              </div>
                            </td>
                            <td>
                              <div class="d-flex flex-row justify-content-center">
                                <a href="../admin/editproject.php?id_project=<?php echo $row['id_project']; ?>"><input type="submit" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-white" value="Edit"></a>
                                <a href="../admin/hapusproject.php?id_project=<?php echo $row['id_project']; ?>"><input type="submit" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-dark" value="Hapus"></a>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                    <?php
                      }
                    } else {
                      echo "0 results";
                    }
                    ?>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
        <?php include "footer.php";?>
      </div>
  </main>
</body>

</html>