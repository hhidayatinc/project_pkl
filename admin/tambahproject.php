<?php 
 
session_start();
 
if (!isset($_SESSION['username'])) {
    header("Location: ../admin/tambahproject.php");
}
 
?>
<!DOCTYPE html>
<html lang="en">
<?php include "head.php";?>

<body>
<?php 
  include '../koneksi.php';
  
  if(isset($_POST["add"])){
	
    
    $nama_kategori=$_POST['nama_kategori'];
    $deskripsi=$_POST['deskripsi'];
    
    $nama_file = $_FILES['gambar']['name'];
    $ukuran_file = $_FILES['gambar']['size'];
    $tipe_file = $_FILES['gambar']['type'];
    $tmp_file = $_FILES['gambar']['tmp_name'];
    
    // Set path folder tempat menyimpan gambarnya
    $path = "../assets/images/".$nama_file;
    if($tipe_file == "image/jpeg" || $tipe_file == "image/png"){ // Cek apakah tipe file yang diupload adalah JPG / JPEG / PNG
        // Jika tipe file yang diupload JPG / JPEG / PNG, lakukan :
        if($ukuran_file <= 1000000){ // Cek apakah ukuran file yang diupload kurang dari sama dengan 1MB
          // Jika ukuran file kurang dari sama dengan 1MB, lakukan :
          // Proses upload
          if(move_uploaded_file($tmp_file, $path)){ // Cek apakah gambar berhasil diupload atau tidak
            // Jika gambar berhasil diupload, Lakukan :  
            // Proses simpan ke Database
    
            $sqlsimpan = mysqli_query($conn, "INSERT INTO project (nama_kategori, deskripsi, gambar)
            VALUES
            ('$nama_kategori', '$deskripsi', '$nama_file')") or die(mysqli_error($conn));
    
    
    if($sqlsimpan){
        echo "<script>alert('Successfully Updated!')</script>";
        header("location:../admin/tabelproject.php");
    } else {
        echo mysqli_error();
       
    }
    
}else{
    // Jika gambar gagal diupload, Lakukan :
    echo "<script>alert('Maaf, Gambar gagal untuk diupload!!!')</script>";
    header("location:?page=../admin/tambahproject.php");
}
}else{
  // Jika ukuran file lebih dari 1MB, lakukan :
    echo "<script>alert('Maaf, Ukuran gambar yang diupload tidak boleh lebih dari 1MB!!!')</script>";
     header("location:?page=../admin/tambahproject.php");
}
}else{
// Jika tipe file yang diupload bukan JPG / JPEG / PNG, lakukan :
echo "<script>alert('Maaf, Tipe gambar yang diupload harus JPG / JPEG / PNG.!!!')</script>";
header("location:?page=../admin/tambahproject.php");
}
}
  ?>

    <body>
        <?php include "navbar.php";?>

        <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
            <!-- Navbar -->
            <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
                <div class="container-fluid py-1 px-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Tabels</a></li>
                            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Project</li>
                        </ol>
                        <h6 class="font-weight-bolder mb-0">Tambah Data</h6>
                    </nav>
                    <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                        <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                            <div class="input-group">
                                <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" placeholder="Type here...">
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->


            <div class="container-fluid ">


                <div class="row mt-3">
                    <div class="row my-3">
                        <div class="col-lg-12 col-md-10 mb-md-0 mb-8">
                            <div class="card">
                                
                                <div class="card-body px-0 pb-2">

                                    <form class="row g-3" style="margin:10px;" action="" method="POST" enctype="multipart/form-data">
                                    
                                        <div class="col-12">
                                            <label for="inputEmail4" class="form-label">Nama</label>
                                            <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" >
                                        </div><br>
                                        <div class="col-12">
                                            <label for="inputPassword4" class="form-label">Deskripsi</label>
                                            <textarea type="text" class="form-control" id="deskripsi" name="deskripsi" ></textarea>
                                        </div>
                                        <div class="form-group">
                                        <label>Gambar</label>
                                        <input type="file" class="form-control" name="gambar">


                                    </div>

                                    
                                    <div class="col-12" style="margin-top: 30px;">
                                        <button type="submit" name="add" class="btn btn-primary">Tambah Data</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php include "footer.php";?>
                </div>
        </main>
    </body>

</html>