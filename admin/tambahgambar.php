<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("Location: ../admin/tambahgambar.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<?php include "head.php";?>
<?php include '../koneksi.php';
//menangkap data yang dikirim dari form login

if (isset($_POST["add"])) { //jika tombol login di klik
    $id_gambar = $_POST['id_gambar'];
    $id_project = $_POST['id_project'];

    $nama_file = $_FILES['gambar']['name'];
    $ukuran_file = $_FILES['gambar']['size'];
    $tipe_file = $_FILES['gambar']['type'];
    $tmp_file = $_FILES['gambar']['tmp_name'];

    // Set path folder tempat menyimpan gambarnya
    $path = "../assets/images/" . $nama_file;
    if ($tipe_file == "image/jpeg" || $tipe_file == "image/png" || $tipe_file == "image/webp") { // Cek apakah tipe file yang diupload adalah JPG / JPEG / PNG
        // Jika tipe file yang diupload JPG / JPEG / PNG, lakukan :
        if ($ukuran_file <= 5000000) { // Cek apakah ukuran file yang diupload kurang dari sama dengan 1MB
            // Jika ukuran file kurang dari sama dengan 1MB, lakukan :
            // Proses upload
            if (move_uploaded_file($tmp_file, $path)) { // Cek apakah gambar berhasil diupload atau tidak
                // Jika gambar berhasil diupload, Lakukan :  
                // Proses simpan ke Database

                $sqlsimpan = mysqli_query($conn, "INSERT INTO gambar (id_gambar, id_project, gambar)
	            VALUES
	                ('$id_gambar', '$id_project', '$nama_file')") or die(mysqli_error($conn));

                if ($sqlsimpan) { // Cek jika proses simpan ke database sukses atau tidak
                    // Jika Sukses, Lakukan :
                    echo "<script>alert('data berhasil disimpan!!!')</script>";
                    header("location:../admin/tabelgambar.php"); // Redirectke halaman index.php
                    // return 'dprinting.html';
                } else {
                    // Jika Gagal, Lakukan :
                    echo mysqli_error();
                }
            } else {
                // Jika gambar gagal diupload, Lakukan :
                echo "<script>alert('Maaf, Gambar gagal untuk diupload!!!')</script>";
                header("location:?page=../admin/tambahgambar.php");
            }
        } else {
            // Jika ukuran file lebih dari 1MB, lakukan :
            echo "<script>alert('Maaf, Ukuran gambar yang diupload tidak boleh lebih dari 1MB!!!')</script>";
            header("location:?page=../admin/tambahgambar.php");
        }
    } else {
        // Jika tipe file yang diupload bukan JPG / JPEG / PNG, lakukan :
        echo "<script>alert('Maaf, Tipe gambar yang diupload harus JPG / JPEG / PNG.!!!')</script>";
        header("location:?page=../admin/tambahgambar.php");
    }
}
?>

<body>
    <?php include "navbar.php";?>

    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <!-- Navbar -->
        <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
            <div class="container-fluid py-1 px-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                        <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Tabels</a></li>
                        <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Gambar</li>
                    </ol>
                    <h6 class="font-weight-bolder mb-0">Tambah Data</h6>
                </nav>
                <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <div class="input-group">
                            <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" placeholder="Type here...">
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->


        <div class="container-fluid">
            <div class="row mt-3">
                <div class="row my-3">
                    <div class="col-lg-12 col-md-10 mb-md-0 mb-8">
                        <div class="card">
                           
                            <div class="card-body px-0 pb-2">

                                <form class="row g-3" style="margin-left:10px;" action="" method="POST" enctype="multipart/form-data">
                                
                                    <div class="col-md-6 form-group">
                                        <label class="text-black font-weight-bold" for="kamar">Kategori Produk</label>
                                        <select required name="id_project" id="id_project" class="form-control" >
                                        <?php
                                        $sql = mysqli_query($conn, "SELECT * FROM project");
                                        foreach ($sql as $value) {
                                            ?>
                                            <option value="<?= $value['id_project']; ?>"><?= $value['nama_kategori']; ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                    </div>
                                
                                    <div class="form-group">
                                        <label>Gambar</label>
                                        <input type="file" class="form-control" name="gambar">
                                    </div>

                                    
                                    <div class="col-12" style="margin-top: 30px;">
                                        <button type="submit" name="add" class="btn btn-primary">Tambah Data</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <?php include "footer.php";?>
            </div>
    </main>
</body>
</html>