<?php

session_start();

if (!isset($_SESSION['username'])) {
  header("Location: ../admin/tabelgambar.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<?php include "head.php";?>
<!-- body -->

<body>
  <?php include "navbar.php";?>

  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Tabels</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Gambar</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Gambar</h6>
        </nav>
        
        
      </div>
    </nav>
    <!-- End Navbar -->


    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
          <div class="col-4 text-start" style="margin-left:15px;">
            <a href="../admin/tambahgambar.php">
              <button type="button" class="fas fa-plus icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
              </button>
            </a>
          </div>
        </div>
      </div>

      <div class="row mt-3">
        <div class="row my-3">
          <div class="col-lg-12 col-md-10 mb-md-0 mb-8">
            <div class="card">
              <div class="card-body px-0 pb-2">
                <div class="table-responsive" style="margin-left:15px;">
                  <table class="table align-items-center mb-0">
                    <thead>
                      <tr>
                      
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"><b>Nama</b></th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"><b>Kategori</b></th>
                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"></th>
                      </tr>
                    </thead>

                    <?php
                    include '../koneksi.php';
                    
                    $query = "SELECT gambar.id_gambar, gambar.gambar, project.nama_kategori FROM gambar INNER JOIN project ON gambar.id_project=project.id_project";
                    $result = mysqli_query($conn, $query);
                      
                    if (mysqli_num_rows($result) > 0) {
                      while ($row = mysqli_fetch_assoc($result)) {
                    ?>

                        <tbody>
                          <tr>
                          
                            <td>
                              <div class="d-flex px-2 py-1">
                                <div>
                                  <img src="../assets/images/<?php echo $row['gambar']; ?>" class="avatar avatar-sm me-3" alt="xd">
                                </div>
                                
                              </div>
                            </td>
                            <td>
                              <div class="avatar-group mt-2">
                                <?php echo $row["nama_kategori"] ?>
                              </div>
                            </td>
                            
                            <td>
                              <div class="d-flex flex-row justify-content-center">
                                <a href="../admin/editgambar.php?id_gambar=<?php echo $row['id_gambar'];?>"><input type="submit" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-white" value="Edit"></a>
                                <a href="../admin/hapusgambar.php?id_gambar=<?php echo $row['id_gambar'];?>"><input type="submit" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-dark" value="Hapus"></a>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                    <?php
                      }
                    } else {
                      echo "0 results";
                    }
                  
                    ?>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
        <?php include "footer.php";?>
      </div>
  </main>
</body>

</html>