<?php 
 
session_start();
 
if (!isset($_SESSION['username'])) {
    header("Location: ../admin/editaluminium.php");
}
 
?>
<!DOCTYPE html>
<html lang="en">

<?php include "head.php";?>

<body>
<?php 
  include '../koneksi.php';

  $id_aluminium = $_GET['id_aluminium'];
  $qry = mysqli_query($conn, "SELECT * FROM aluminium WHERE id_aluminium='$id_aluminium'");
  $row = mysqli_fetch_array($qry);
  
  if(isset($_POST["submit"])){
	
    $nama_produk=$_POST['nama_produk'];
    $harga_produk=$_POST['harga_produk'];
    $deskripsi=$_POST['deskripsi'];
    
    $edit=mysqli_query($conn, "UPDATE aluminium SET 
    nama_produk='$nama_produk',
    harga_produk='$harga_produk',
    deskripsi='$deskripsi'
    WHERE id_aluminium='$id_aluminium'") or die(mysqli_error($conn));
    
    
    if($edit){
        echo "<script>alert('Successfully Updated!')</script>";
        header("location:../admin/tabelaluminium.php");
    } else {
        echo mysqli_error();
       
    }
    
}
  ?>

    <body>
        <?php include "navbar.php";?>

        <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
            <!-- Navbar -->
            <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
                <div class="container-fluid py-1 px-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Tabels</a></li>
                            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Aluminium</li>
                        </ol>
                        <h6 class="font-weight-bolder mb-0">Edit Data</h6>
                    </nav>
                    <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                        <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                            <div class="input-group">
                                <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" placeholder="Type here...">
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->


            <div class="container-fluid py-4">


                <div class="row mt-3">
                    <div class="row my-3">
                        <div class="col-lg-12 col-md-10 mb-md-0 mb-8">
                            <div class="card">
                                
                                <div class="card-body px-0 pb-2">

                                    <form class="row g-3" style="margin:10px;" action="" method="POST" enctype="multipart/form-data">
                                    
                                        <div class="col-md-6">
                                            <label for="inputEmail4" class="form-label">Nama</label>
                                            <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="<?php echo $row['nama_produk']; ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="inputPassword4" class="form-label">Harga Produk</label>
                                            <input type="text" class="form-control" id="harga_produk" name="harga_produk" value="<?php echo $row['harga_produk']; ?>">
                                        </div>
                                        <div class="col-12">
                                            <label for="inputAddress2" class="form-label">Deskripsi</label>
                                            <textarea  rows="5" type="text" class="form-control" id="deskripsi" name="deskripsi" value="<?php echo $row['deskripsi']; ?>"></textarea>
                                        </div>
                                        <div class="col-12" style="margin-top: 30px;">
                                            <button type="submit" name="submit" class="btn btn-primary">Update Data</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php include "footer.php";?>
                </div>
        </main>
    </body>

</html>