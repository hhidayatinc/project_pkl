<header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        
                        <a href="index.html" class="logo"> SBMB
                        </a>
                        
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="index.php">Home</a></li>
                            <li class="scroll-to-section"><a href="about.php" >About</a></li>
                            <li class="scroll-to-section"><a href="projects.php">Projects</a></li>
                            <li class="submenu">
                                <a href="javascript:;">Product</a>
                                <ul>
                                    <li><a href="filterrods.php">Filterrods</a></li>
                                    <li><a href="alumuniumpaper.php">alumunium Paper</a></li>
                                    <li><a href="dprinting.php">Digital Printing</a></li>
                                </ul>
                            </li>
                            <li class="scroll-to-section"><a href="contact.php">Contact Us</a></li> 
                            <li class="scroll-to-section"><a href="admin/login.php">Login</a></li> 
                            
                        </ul>        
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>