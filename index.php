
<!DOCTYPE html>
<html lang="en">

    <?php include 'head.php';?>

    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="42d9fc4a-fc9d-4d08-8eb5-660d948071fc";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <?php include 'header.php'; ?>
    <!-- ***** Header Area End ***** -->



    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="assets/images/slide-05.png" alt=""> 
                    <div class="text-content">

                        <h3 style="font-size: 15pt;color: black; text-align: right; font-weight: bold;">WELCOME</h3>
                        <h5 style="font-size: 25pt; color: black; text-align: right; font-weight: bolder;">PT. Sarana Berkah Maju Bersama</h5>
                        
                </div>
            </div>
           
        </div>
    </div>
    <div class="scroll-down scroll-to-section"><a href="#about"><i class="fa fa-arrow-down"></i></a></div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>About Us</h6>
                            <h2>Company Philosophy</h2>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="service-item">
                                    <img src="assets/images/service-item-01.png" alt="">
                                    <h4>Trust</h4>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="service-item">
                                    <img src="assets/images/service-item-01.png" alt="">
                                    <h4>Innovation</h4>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="service-item">
                                    <img src="assets/images/contact-info-03.png" alt="">
                                    <h4>Quality</h4>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <a href="about.html" class="main-button-icon">
                                    Learn More <i class="fa fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="right-text-content">
                        <p style="color: black; font-size: medium; text-align: justify;"><a rel="nofollow noopener"
                                href="https://www.google.com/maps/place/PT+Sarana+Berkah+Maju+Bersama/@-8.0255829,112.642608,15z/data=!4m2!3m1!1s0x0:0x3451d0a5545123ce?sa=X&ved=2ahUKEwjnu__Wq7v1AhXCUGwGHSyMDhsQ_BJ6BAgkEAU"
                                target="_parent">
                                PT. Sarana Berkah Maju Bersama </a>
                            atau bisa disingkat SBMB adalah perusahaan yang bergerak di bidang secondary
                            tobacco product <br>seperti : Filter Rod dan Aluminium Foil Paper (Kertas Grenjeng Rokok).
                            <br><br>Tidak hanya itu saja,
                            SBMB juga bergerak dibidang bisnis Digital
                            Printing Product. Dengan kualitas bahan material
                            dan mesin yang canggih serta didukung kualitas
                            Sumber Daya Manusia yang baik, SBMB mampu
                            memberikan pengalaman produk berkualitas
                            kepada konsumen. SBMB memiliki prinsip bahwa
                            kepercayaan konsumen dan kualitas produk
                            adalah dua hal yang berkesinambungan.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <div class="features-item">
                        <div class="features-icon">
                            <img src="assets/images/features-icon-1.png" alt="">
                        </div>
                        <div class="features-content">
                            <h4 style="font-size: large;">Filter Rod</h4>
                            <a href="filterrods.html" class="text-button-icon">
                                Learn More <i class="fa fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter bottom move 30px over 0.6s after 0.4s">
                    <div class="features-item">
                        <div class="features-icon">
                            <img src="assets/images/features-icon-1.png" alt="">
                        </div>
                        <div class="features-content">
                            <h4 style="font-size: large;">Alumunium Paper</h4>
                            <a href="alumuniumpaper.html" class="text-button-icon">
                                Learn More <i class="fa fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <div class="features-item">
                        <div class="features-icon">
                            <img src="assets/images/features-icon-1.png" alt="">
                        </div>
                        <div class="features-content">
                            <h4 style="font-size: large;">Digital Printing</h4>
                            <a href="dprinting.html" class="text-button-icon">
                                Learn More <i class="fa fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->

    <!-- ***** VisiMisi Area Starts ***** -->
    <section class="section" id="visimisi">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="left-text-content">
                        <div class="section-heading" style="text-align: center;">
                            <h2>Visi</h2>
                        </div>
                        <div class="right-text-content">
                            <p style="color: black; font-size: medium; text-align: center;"><br>Menunjang kebutuhan
                                secondary tobacco
                                product di Jawa Timur<br>khususnya dan seluruh
                                pasar Indonesia</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12" style="text-align: center;">
                    <div class="section-heading">
                        <h2>Misi</h2>
                    </div>
                    <div class="right-text-content">
                        <p style="color: black; font-size: medium; text-align: center;"><br>Usaha kami adalah
                            menyediakan produk yang
                            menjadi bagian dari secondary tobacco product
                            dengan standar operasional dan jaminan mutu
                            yang baik serta harga yang bisa bersaing dalam
                            pasar</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** VisiMisi Area Ends ***** -->




    <!-- ***** Projects Area Starts ***** -->
    <section class="section" id="projects">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="section-heading">
                        <h6>Our Projects</h6>
                        <h2>Some of our latest projects</h2>
                    </div>
                    <div class="filters">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".fil">Filterrods</li>
                            <li data-filter=".alu">Aluminium Paper</li>
                            <li data-filter=".pri">Digital Printing</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="filters-content">
                        <div class="row grid">
                        <?php include "koneksi.php";
                            $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=1 LIMIT 3');
                            $result = array();
                            while ($data = mysqli_fetch_array($query)){
                            $result[] = $data;
                            } foreach ($result as $row){
                        ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 all fil">
                                <div class="item">
                                    <a href="assets/images/<?php echo $row['gambar'];?>" data-lightbox="image-1"
                                        data-title="Our Projects"><img src="assets/images/<?php echo $row['gambar']; ?>" alt=""></a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php include "koneksi.php";
                            $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=3 LIMIT 3');
                            $result = array();
                            while ($data = mysqli_fetch_array($query)){
                            $result[] = $data;
                            }foreach ($result as $row){
                        ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 all pri">
                                <div class="item">
                                    <a href="assets/images/<?php echo $row['gambar'];?>" data-lightbox="image-1"
                                        data-title="Our Projects"><img src="assets/images/<?php echo $row['gambar'];?>" alt=""></a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php include "koneksi.php";
                            $query = mysqli_query($conn, 'SELECT * FROM gambar WHERE id_project=2 LIMIT 3');
                            $result = array();
                            while ($data = mysqli_fetch_array($query)){
                            $result[] = $data;
                            }
                            foreach ($result as $row){
                        ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 all alu">
                                <div class="item">
                                    <a href="assets/images/<?php echo $row['gambar'];?>" data-lightbox="image-1"
                                        data-title="Our Projects"><img src="assets/images/<?php echo $row['gambar'];?>" alt=""></a>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Projects Area Ends ***** -->

    <!-- ***** Footer Start ***** -->
    <?php include 'footer.php'; ?>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

    <script>

        $(function () {
            var selectedClass = "";
            $("p").click(function () {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut();
                setTimeout(function () {
                    $("." + selectedClass).fadeIn();
                    $("#portfolio").fadeTo(50, 1);
                }, 500);

            });
        });

    </script>

</body>

</html>