<!DOCTYPE html>
<html lang="en">
   
<?php include 'head.php';?>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <?php include 'header.php';?>
    <!-- ***** Header Area End ***** -->
    
    
    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
          <!-- Item -->
          <div class="item">
            <div class="img-fill">
                <img src="assets/images/slide-04.png" alt="" style="opacity: 0.8;">
                <div class="text-content">
                    <h3>ABOUT US</h3>
                    <h5>PT. Sarana Berkah Maju Bersama</h5>
                    <p style="font-size: 20pt; color:white; margin-top: -20px;">"better quality, better taste"</p>
                </div>
            </div>
          </div>
          <!-- // Item -->
          
          
        </div>
    </div>
    <div class="scroll-down scroll-to-section"><a href="#about"><i class="fa fa-arrow-down"></i></a></div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xs-12">
                    <div class="section-heading">
                        <h6>History </h6>
                        <h2>Our Journey</h2>
                    </div>
                    <div class="right-text-content"
                        data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <p style="color: black; text-align: justify; font-size: large;">Sejarah <a rel="nofollow noopener" href="https://goo.gl/maps/dJFj6bz6YtHrN74Y6" target="_parent">PT. Sarana Berkah Maju Bersama </a> 
                            atau SBMB tidak bisa dipisahkan dari 
                            industri rokok yang ada di Jawa Timur khususnya 
                            Kota Malang. Pada awal mula berdiri di tahun 
                            2011, SBMB adalah perusahaan Joint Venture
                            dari 2 entitas lebih yang bertujuan untuk 
                            memenuhi kebutuhan Filter Rod dari dua grup 
                            rokok yang ada di Malang yaitu ‘Gudang Baru 
                            Group’ dan ‘Cakra Guna Karya Nusa’. Pada 
                            tahun 2017, Gudang Baru Group membeli 
                            seluruh saham yang dimiliki oleh Cakra Guna 
                            Karya. Sehingga Gudang Baru Group saat ini 
                            adalah pemilik saham penuh dari SBMB dengan 
                            manajemen baru dari Gudang Baru Group.
                            <br><br>Pada perjalanannya, SBMB melakukan 
                            diversifikasi produk yang semula hanya 
                            memproduksi Filter Rod hingga akhirnya 
                            berkembang dalam bisnis Aluminium Foil Paper 
                            atau bisa disebut Kertas Grenjeng Rokok. SBMB 
                            memiliki 2 lokasi pabrik (Plant) dengan kapasitas 
                            produksi yang terus meningkat setiap tahunnya. 
                            Lokasi pertama adalah Kantor Pusat sekaligus 
                            Pabrik Rod Filter dan Digital Printing (Plant 
                            Gadang). Lokasi kedua adalah Kantor Cabang 
                            sekaligus Pabrik Aluminium Foil Paper (Plant 
                            Sukoraharjo).</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about" style="margin-top: 0pt;">
        <div class="container">
            <div class="row">
                <div>
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>More About Us</h6>
                            <h2>Company Philosophy</h2>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                                data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                                <div class="service-item">
                                    <img src="assets/images/service-item-01.png" alt="">
                                    <p style="color: black; font-size: 18pt;"><b>Kepercayaan</b></p>
                                    <br>
                                    <div class="text-content" style="margin-left: 50px;">
                                        <p style="color: black; font-size: 12pt; text-align: justify;">
                                            Kami menjaga kepercayaan mitra kami dengan 
                                            selalu melakukan usaha perbaikan dan menjaga 
                                            kualitas produk untuk tetap menjadi yang terbaik 
                                            di pasarnya
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                                data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                                <div class="service-item">
                                    <img src="assets/images/service-item-01.png" alt="">
                                    <p style="color: black; font-size: 18pt;"><b>Inovasi</b></p>
                                    <br>
                                    <div class="text-content" style="margin-left: 50px; ">
                                        <p style="color: black; font-size: 12pt; text-align: justify;">
                                            Kami mendorong batasan yang ada saat ini 
                                            dengan inovasi untuk memenuhi kebutuhan 
                                            pasar
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                                data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                                <div class="service-item">
                                    <img src="assets/images/contact-info-03.png" alt="">
                                    <p style="color: black; font-size: 18pt;"><b>Kualitas</b></p>
                                    <br>
                                    <div class="text-content" style="margin-left: 50px;  ">
                                        <p style="color: black; font-size: 12pt; text-align: justify;">
                                            Kami percaya, nilai dari sebuah kualitas adalah 
                                            memberikan pengalaman terbaik untuk mitra 
                                            kami
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->

    <!-- ***** Visi Misi Area Starts ***** -->
    <section class="section" id="about" >
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12"
                    data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <div class="left-text-content">
                            <div class="section-heading" style="text-align: center;">
                                <h2>Visi</h2>
                            </div>
                            <div class="right-text-content" style="text-align: center;">
                                <br>
                                <p style="color: black; font-size: large; text-align: center;">
                                    Menunjang kebutuhan secondary tobacco
                                    product di Jawa Timur <br>khususnya dan seluruh
                                    pasar Indonesia</p>
                            </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12" style="text-align: center;"
                    data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <div class="section-heading">
                        <h2>Misi</h2>
                    </div>
                    <div class="right-text-content" style="text-align: center;">
                        <br>
                        <p style="color: black; font-size: large; text-align: center;">
                            Usaha kami adalah menyediakan produk yang
                            menjadi bagian dari secondary tobacco product
                            dengan standar operasional dan jaminan mutu
                            yang baik serta harga yang bisa bersaing dalam
                            pasar</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Visi Misi Area Ends ***** -->

      <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xs-12">
                    <div class="section-heading">
                        <h6>Get To Know</h6>
                        <h2>Plant Operation</h2>
                        <br>
                    </div>
                    <div class="image">
                        <img src="assets/images/operation.png" style="width: 1000px; height: 500px; display: block; margin: auto;">
                    </div>
                    <div class="right-text-content"
                        data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <br>
                        <p style="color: black; text-align: justify; font-size: large;"><a rel="nofollow noopener" href="https://goo.gl/maps/dJFj6bz6YtHrN74Y6" target="_parent">PT. Sarana Berkah Maju Bersama </a> 
                            atau SBMB memiliki 2 pabrik yang beroperasi di Jawa Timur, yaitu Pabrik Gadang 
                            dan Pabrik Sukoraharjo. Pabrik Gadang, didirikan bersamaan dengan 
                            berdirinya SBMB pada tahun 2011 yang berlokasi di Jalan Mayjend 
                            Sungkono, Nomor 36, Kedungkandang, Kota Malang. Pabrik Gadang 
                            memproduksi Filter Rod dengan kapasitas per bulannya mencapai 14.250 
                            tray filter reguler dan 17.250 tray filter bold. Produk lain dari Pabrik Gadang 
                            yaitu Digital Printing. Untuk Pabrik Sukoraharjo, didirikan pada tahun 2021 
                            dan berlokasi di Jalan Raya Sukoraharjo, Kepanjen, Kabupaten Malang. 
                            Pendirian Pabrik Sukoraharjo guna memenuhi permintaan pasar yaitu 
                            Aluminium Foil Paper (Kertas Grenjeng Rokok) dengan produksi kapasitas 
                            sebesar 6 ton setiap bulannya.
                           
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->
    
    
    <!-- ***** Footer Start ***** -->
    <?php include 'footer.php'; ?>
    

    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    <script src="assets/js/slick.js"></script> 
    <script src="assets/js/lightbox.js"></script> 
    <script src="assets/js/isotope.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

    <script>

        $(function() {
            var selectedClass = "";
            $("p").click(function(){
            selectedClass = $(this).attr("data-rel");
            $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("."+selectedClass).fadeOut();
            setTimeout(function() {
              $("."+selectedClass).fadeIn();
              $("#portfolio").fadeTo(50, 1);
            }, 500);
                
            });
        });

    </script>

  </body>
</html>