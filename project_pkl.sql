-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2022 at 05:31 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_pkl`
--

-- --------------------------------------------------------

--
-- Table structure for table `aluminium`
--

CREATE TABLE `aluminium` (
  `id_aluminium` varchar(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aluminium`
--

INSERT INTO `aluminium` (`id_aluminium`, `id_project`, `nama_produk`, `harga_produk`, `deskripsi`) VALUES
('2', 2, 'Alumunium Foil', 0, 'Aluminium foil adalah film soft metal.Aluminium foil adalah aluminium yang disiapkan dalam daun logam tipis dengan ketebalan kurang dari 0,2 mm (7,9 mils). Foil rumah tangga standar biasanya memiliki tebal 0,016 mm (0,63 mil), dan foil rumah tangga tugas berat biasanya 0,024 mm (0,94 mil). Foilnya lentur, dan dapat dengan mudah ditekuk atau dililitkan di sekitar objek. Foil tipis rapuh dan terkadang dilaminasi dengan bahan lain seperti plastik atau kertas untuk membuatnya lebih kuat dan lebih berguna.'),
('A01', 2, 'Cigarette Inner Box', 0, 'Bahan kemasan bagian dalam kotak rokok adalah kertas aluminium foil, yang juga dikenal sebagai aluminium foil untuk kemasan merokok, aluminium foil untuk kemasan rokok dan aluminium foil rokok. Sebagai kertas aluminium foil untuk kemasan liner rokok, ada terutama kertas aluminium foil yang dibenderkan dan kertas aluminized. Saat ini, kertas aluminium foil rokok komposit telah menjadi bahan dengan potensi pengembangan, ia memiliki penghalang tinggi terhadap cahaya, gas, air dan anti-korosi, efek optik yang baik, perisai yang baik dan reaksi terhadap cahaya dan sinar ultraviolet, dapat secara efektif mencegah efek penuaan cahaya pada kandungan tembakau.');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pesan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `nama`, `email`, `pesan`) VALUES
(1, 'Hidayati Nur Chasanah', 'hidayatinc13@gmail.com', 'Hai ini percobaan'),
(2, 'Hidayati Nur Chasanah', 'hidayatinc13@gmail.com', 'hai');

-- --------------------------------------------------------

--
-- Table structure for table `filterrods`
--

CREATE TABLE `filterrods` (
  `id_filterrods` varchar(50) NOT NULL,
  `id_project` int(11) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `diameter` varchar(50) NOT NULL,
  `bahan_produk` varchar(50) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `filterrods`
--

INSERT INTO `filterrods` (`id_filterrods`, `id_project`, `nama_produk`, `harga_produk`, `diameter`, `bahan_produk`, `deskripsi`, `gambar`) VALUES
('1', 1, 'Filterrods Mild ', 12000, '5.80 mm – 7.00 mm', 'Porositas ', 'Filterrods Mild Porositas adalah jenis filterrods yang dipakai untuk produk rokok sigaret filter Mild/Slim dimana Plug Wrap yang digunakan adalah jenis Porositas.', 'bmsfilterrods2.jpg'),
('2', 1, 'Filterrods Reguler', 0, '7.60 mm – 8 .00 mm.', 'Cellulose Acetate / Acetate Tow.', 'Filterrods Regular adalah jenis filterrods yang dipergunakan untuk produk rokok sigaret filter. Filterrods berfungsi untuk menyaring/mengurangi kandungan tar dan nikotin yang terdapat pada rokok tersebut.', 'bmsfilterrods3.jpg'),
('3', 1, 'Filterrods Bold', 0, '7.00 mm. - 7.80 mm.', 'Acetate Tow.', 'Filterrods Bold adalah jenis filterrods yang dipakai untuk produk rokok dengan diameter paling tebal diantara mild dan reguler.', 'bmsfilterrods1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gambar`
--

CREATE TABLE `gambar` (
  `id_gambar` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gambar`
--

INSERT INTO `gambar` (`id_gambar`, `id_project`, `gambar`) VALUES
(3, 1, 'bmsfilterrods4.jpg'),
(4, 1, 'bmsfilterrods1.jpg'),
(5, 2, 'paper1.jpg'),
(6, 2, 'paper2.jpg'),
(7, 2, 'paper3.jpg'),
(8, 3, 'dprint5.jpg'),
(9, 3, 'dprint3.jpg'),
(11, 1, 'bmsfilterrods2.jpg'),
(12, 3, 'dprint2.jpg'),
(13, 3, 'dprint3.jpg'),
(14, 3, 'dprint4.jpg'),
(16, 2, 'paper4.webp'),
(17, 1, 'bmsfilterrods5.png'),
(18, 1, 'bmsfilterrods6.png'),
(19, 1, 'bmsfilterrods7.png'),
(20, 1, 'bmsfilterrods10.png');

-- --------------------------------------------------------

--
-- Table structure for table `printing`
--

CREATE TABLE `printing` (
  `id_printing` varchar(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga_produk` varchar(11) NOT NULL,
  `gambar` text NOT NULL,
  `bahan_produk` varchar(50) NOT NULL,
  `ukuran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `printing`
--

INSERT INTO `printing` (`id_printing`, `id_project`, `nama_produk`, `harga_produk`, `gambar`, `bahan_produk`, `ukuran`) VALUES
('1', 3, 'Banner', '7000/14000', 'dprint1.jpg', 'standart, tebal', '40x50 cm'),
('2', 3, 'Spanduk', '18000', 'dprint2.jpg', 'semi super', '/m'),
('3', 3, 'Sticker', '15000', 'dprint4.jpg', 'cromo', 'A3, qty : 1-5'),
('4', 3, 'Kartu Nama', '38000/box', 'dprint5.jpg', 'concorde', '>25, 2 sisi'),
('5', 3, 'Cetak Foto', '9000', 'dprint6.jpg', 'kertas foto doff', '10rw / 20x30cm'),
('6', 3, 'Pin', '2300', 'dprint7.jpg', 'pin peniti', '4,4cm'),
('7', 3, 'Cutting Sticker', '24000/m', 'dprint10.jpg', 'glossy', '1m');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `bahan_produk` varchar(50) NOT NULL,
  `ukuran_produk` varchar(50) NOT NULL,
  `gambar` text NOT NULL,
  `harga_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_kategori`, `nama_produk`, `bahan_produk`, `ukuran_produk`, `gambar`, `harga_produk`) VALUES
(3, 3, 'Cutting Stiker', 'Matte', '', 'dprint10.jpg', 12000),
(4, 1, 'Filterrods Mild', 'Mate', '1x1', 'bmsfilterrods2.jpg', 12000);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id_project` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id_project`, `nama_kategori`, `deskripsi`, `gambar`) VALUES
(1, 'Filterrods', 'Filterrods Industry', 'bmsfilterrods3.jpg'),
(2, 'Aluminium Paper', 'Grenjeng Paper', 'paper.jpg'),
(3, 'Printing', 'Digital Printing / Layanan Percetakan', 'evergreenprinting1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'adminsbmb', '31f2ed1d103e117a362be031f9962e03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aluminium`
--
ALTER TABLE `aluminium`
  ADD PRIMARY KEY (`id_aluminium`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filterrods`
--
ALTER TABLE `filterrods`
  ADD PRIMARY KEY (`id_filterrods`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id_gambar`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `printing`
--
ALTER TABLE `printing`
  ADD PRIMARY KEY (`id_printing`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id_project`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id_gambar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id_project` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aluminium`
--
ALTER TABLE `aluminium`
  ADD CONSTRAINT `aluminium_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`);

--
-- Constraints for table `filterrods`
--
ALTER TABLE `filterrods`
  ADD CONSTRAINT `filterrods_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`);

--
-- Constraints for table `gambar`
--
ALTER TABLE `gambar`
  ADD CONSTRAINT `gambar_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`);

--
-- Constraints for table `printing`
--
ALTER TABLE `printing`
  ADD CONSTRAINT `printing_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
