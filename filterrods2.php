<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'; ?>
<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <?php include 'header.php'; ?>
    <!-- ***** Header Area End ***** -->



    <!-- ***** Main Banner Area Start ***** -->
    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
            <!-- Item -->
            <div class="item">
                <div class="img-fill">
                    <img src="assets/images/bmsfilterrods5.jpg" alt="">
                    <div class="text-content">
                        <h3>PRODUCTS</h3>
                        <h5>SBMB Filterrods #2 Filterrods Reguler</h5>
                    </div>
                </div>
            </div>
            <!-- // Item -->


        </div>
    </div>
    <div class="scroll-down scroll-to-section"><a href="#about"><i class="fa fa-arrow-down"></i></a></div>
    <!-- ***** Main Banner Area End ***** -->

    <!-- ***** About Area Starts ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-xs-12">
                    <div class="section-heading">
                        <h2>Overview </h2>
                        <!-- <h2>SBMB Digital Printing</h2> -->
                    </div>
                    <div class="right-text-content">
                        <p style="color: black; text-align: justify; font-size: large;">
                            <b>“Filterrods Regular menjadi produk andalan untuk penjualan di wilayah Indonesia, karena lebih banyak pabrik-pabrik memproduksi rokok kretak sigaret filter".</b><br><br>

                            Filterrods Regular adalah jenis filterrods yang dipergunakan untuk produk rokok sigaret filter. Filterrods berfungsi untuk menyaring/mengurangi kandungan tar dan nikotin yang terdapat pada rokok tersebut.<br>
                            
                            Bahan baku utama yang dipergunakan adalah Cellulose Acetate atau lebih dikenal dengan nama Acetate Tow.<br>
                            
                            Untuk bahan baku penunjangnya menggunakan Plug Wrap sebagai pembungkus dan Treacitine yang berfungsi untuk mangikat filament-filamen Acetate Tow. Filterrods Regular banyak digunakan untuk produksi rokok sigaret filter yang berdiameter besar antara 7.60 mm – 8 .00 mm.<br><br>
                            
                            Harga & Ukuran : (sesuai permintaan)<br>     
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

    <!-- gallery -->
    <!-- <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="section-heading" style="text-align: center;">
                <br>
                <h2>Gallery Filterrods</h2>
            </div>
            <div class="subscribe-content" style="text-align: center;">
                <p>berikut merupakan kumpulan gambar produk dan fasilitas dari filterrods</p>
                <br>
            </div>
        </div>
    </div>

    <ul class="gallery_box">
        <li>
            <a href="#0"><img src="assets/images/bmsgallery1.jpg"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery4.jpg"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery3.jpg"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery10.png"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery5.png"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery6.png"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery7.png"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery8.png"></a>
        </li>
        <li>
            <a href="#0"><img src="assets/images/bmsgallery9.png"></a>
        </li>
        
    </ul> -->
    <!-- gallery end -->

    <!-- ***** Footer Start ***** -->
    <footer style="height: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="left-text-content">
                        <p>Copyright &copy; 2020 Breezed Co., Ltd.

                            - Design: <a rel="nofollow noopener" href="https://templatemo.com">TemplateMo</a></p>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="right-text-content">
                        <ul class="social-icons">
                            <li>
                                <p>Contact Us</p>
                            </li>
                            <li><a rel="nofollow" href="https://fb.com/templatemo"><i class="fa fa-envelope"></i></a></li>
                            <li><a rel="nofollow" href="https://fb.com/templatemo"><i class="fa fa-phone"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

    <script>

        $(function () {
            var selectedClass = "";
            $("p").click(function () {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut();
                setTimeout(function () {
                    $("." + selectedClass).fadeIn();
                    $("#portfolio").fadeTo(50, 1);
                }, 500);

            });
        });

    </script>

</body>

</html>