<footer style="height: 0px;">
        <div class="container"  >
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="left-text-content">
                        <p>Copyright &copy; PT. Sarana Berkah Maju Bersama
                            <!-- - Template by 2020 Breezed Co., Ltd.  -->
                            - Intern Team</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li>
                                    <p>Contact Us</p>
                                </li>
                                <li><a rel="nofollow" href="mailto:bcsbmb@gmail.com?Subject=Message From User"><i class="fa fa-envelope"></i></a>
                                </li>
                                <li><a rel="nofollow" href="tel:0341-751116"><i class="fa fa-phone"></i></a>
                                </li>
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>